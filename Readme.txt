This folder contains Matlab functions for basic conversions, wrapping operations as well as angular velocity integrators on 3-d rotational group.
If you find any mistake or have questions, please contact weixinwang442@gmail.com
Bitbucket: bitbucket.org/weixinwang/rotation3d